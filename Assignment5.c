#include <stdio.h>
#include <stdlib.h>
int Calcatendees(int ticketprice)
{
    int atendees = 120-(ticketprice-15)/5*20;
    return atendees;
}
int calcEarnings(int ticketprice)
{
    int earnings;
    int x = Calcatendees(ticketprice);
    earnings = ticketprice * x;
    return earnings;
}
int Calccost(int ticketprice)
{
    int x = Calcatendees(ticketprice);
    int cost = 500 + (3 * x);
    return cost;
}
int Calcprofit(int ticketprice)
{
    int earning = calcEarnings(ticketprice);
    int cost = Calccost(ticketprice);
    int profit = earning - cost;
    return profit;
}

int main()
{
    int ticketprice;
	printf("\nExpected Profit for Ticket Prices: \n\n");
	for(ticketprice=5;ticketprice<50;ticketprice+=5)
	{
		printf("Ticket Price = Rs.%d for Profit = Rs.%d\n\n",ticketprice,Calcprofit(ticketprice));

    }
		return 0;
}

