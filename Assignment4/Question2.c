#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num;
    printf("Enter number: ");
    scanf("%d",&num);
    int prime;
    for(int i=2;num/2 >= i;i++)
    {
        if(num%i == 0)
        {
            prime = 0;
            break;
        }
    }
    if(num == 1)
    {
        printf("1 is neither a prime or no composite");
    }else
    {
        if(prime == 0)
        {
        printf("not a prime number");
        }else
        {
        printf("prime number");
        }
    }

    return 0;
}
