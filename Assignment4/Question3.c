#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num,reverse_num;
    printf("Enter number ");
    scanf("%d",&num);
    while(num > 0)
    {
        reverse_num = num%10;
        num = num/10;
        printf("%d",reverse_num);
    }
    return 0;
}
