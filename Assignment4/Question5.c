#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n;
    printf("Enter number: ");
    scanf("%d",&n);
    for(int i =1;i <=n ; i++)
    {
        for(int j =1; j <= 12; j++)
        {
            printf("\n");
            printf("%d ", i*j);
        }
        printf("\n");
    }
    return 0;
}
