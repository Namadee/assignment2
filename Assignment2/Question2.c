#include <stdio.h>
#include <stdlib.h>

int main()
{
    float r,area;
    printf("input radius: ");
    scanf("%f",&r);
    const float pie = 3.14;
    area = pie*r*r;
    printf("Area is %f",area);

    return 0;
}
