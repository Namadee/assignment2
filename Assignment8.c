#include <stdio.h>
#include <stdlib.h>

struct studentDetails
    {
        char name[50];
        char subject[50];
        int marks;

    };

int main()
{
     struct studentDetails s[5];

    for(int i=1;i<6;i++)
    {
        printf("Enter student %d name: ",i);
        scanf("%s",s[i].name);
        printf("Enter student %d subject: ",i);
        scanf("%s",s[i].subject);
        printf("Enter student %d marks: ",i);
        scanf("%d",&s[i].marks);
        printf("\n");
    }

    for(int i=1;i<=5;i++)
    {

        printf("Student %d details\n",i);
        printf("Name: ");
        puts(s[i].name);
        printf("Subject: ");
        puts(s[i].subject);
        printf("marks: %d",s[i].marks);
        printf("\n");

    }
    return 0;
}


