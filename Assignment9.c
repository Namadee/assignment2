#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *fptr;
    fptr = fopen("assignment9.txt","w");
    fprintf(fptr,"UCSC is one of the leading institutes in Sri Lanka for computing studies.");
    fclose(fptr);

    fptr = fopen("assignment9.txt","r");
    char text;
    text = fgetc(fptr);
    while(text != EOF)
    {
        printf("%c",text);
        text = fgetc(fptr);
    }
    fclose(fptr);

    fptr = fopen("assignment9.txt","a");
    fprintf(fptr,"UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
    fclose(fptr);
    return 0;
}
